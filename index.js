require('dotenv').config();
const { default: axios } = require('axios');
const express = require('express');
const cors = require('cors');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(cors());
app.options('*', cors());

const getToken = async (extUserId, userId) => {
  try {
    const params = extUserId ? { extUserId } : { userId };
    if (process.env.SECRET_KEY === 'f3322d59-7a55-4144-abf7-30e7dcfa2283') {
      console.log('You have not changed the secretKey. Since this is a dummy value, this fetch will fail. Please get a set of credentials and replace the secretKey');
    }
    const resp = await axios({
      method: 'post',
      url: process.env.BASE_URL,
      data: params,
      headers: {
        Authorization: `Bearer ${process.env.SECRET_KEY}`,
      },
    });
    return resp.data.data.token;
  } catch (error) {
    console.log(error);
  }
};

app.post('/', (req, res) => {
  const { extUserId, userId } = req.body;
  getToken(extUserId, userId)
    .then(token => {
      res.send(token);
    })
    .catch(e => {
      res.send(e);
    });
});

app.listen(process.env.PORT, () =>
  console.log(`Server started on port ${process.env.PORT}`)
);
